package hello;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

// trying to break things. 
// class demo of starting circlci

public class TestGreeter {

   private Greeter g = new Greeter();

   @Test
   @DisplayName("Test for Empty Name")
   public void testGreeterEmpty() 

   {
      assertEquals(g.getName(),"");
      assertEquals(g.sayHello(),"Hello!");
   }
// real quick change
   @Test
   @DisplayName("Test for Ted")
   public void testGreeterTed() 

   {
      g.setName("Ted");
      assertEquals(g.getName(),"Ted");
      assertEquals(g.sayHello(),"Hello Ted!");
   }


   @Test
   @DisplayName("Test for Name='World'")
   public void testGreeter() 
   {

      g.setName("World");
      assertEquals(g.getName(),"World");
      assertEquals(g.sayHello(),"Hello World!");
   }

    @Test
    @DisplayName("Same Tests")
    public void testAssertionsSame() {
    //test data
        String str_A = new String ("abc");
        String str_B = new String ("abc");
        String str_C = null;
        String str_D = "abc";
        String str_E = "abc";

        int val_1 = 4;
        int val_2 = 3;

        assertSame(str_C,str_C,"Same, Two Null Strings");
        assertSame(str_D,str_E,"Same, Two Identical String Constants"); 
        assertNotSame(str_A,str_B,"Not Same 2, Two Identical Strings Created with New");
    }
   
    @Test
    @DisplayName("False Tests")
        public void testAssertionsFalse() {
        //test data
        String str_A = new String ("abc");
        String str_B = new String ("abc");
        String str_C = null;
        String str_D = "abc";
        String str_E = "abc";

        int val_1 = 4;
        int val_2 = 3;

        assertFalse(str_A==str_B,"False, Two Identical Strings Created with New");
        assertFalse(str_A==str_C,"False, Null and a String Constant");

    }
}
